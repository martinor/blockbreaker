//Crea la clase BlockBreaker
var BlockBreaker = BlockBreaker || {};

//Crea la funcion 'Boot' en la variable BlockBreaker
BlockBreaker.Boot = function() {};

//Inicializa la funcion 'Boot' en un prototipo.
BlockBreaker.Boot.prototype = {
	preload: function() {
		//Carga al loadbar.
		this.load.image('preloadbar', 'asset/UI/loadbar.png');
	},

	create: function() {
		//Fondo del Mientras se cargan los assets.
		this.game.stage.backgroundColor = '#000';

		//Ajustar los elementos al tamaño de la pantalla.
        //Modos disponibles: EXACT_FIT, SHOW_ALL, NO_SCALE
		this.scale.scaleMode = Phaser.ScaleManager.SHOW_ALL;

		//Ajustar el juego horizontalmente.
		this.scale.pageAlignHorizontally = true;
		this.scale.pageAlignVertically = true;

		// Force the orientation in landscape or portrait.
        // * Set first to true to force landscape. 
        // * Set second to true to force portrait.
        this.scale.forceOrientation(true, false);

		//Ajustar automaticamente el tamaño de la pantalla.
		this.scale.setScreenSize(true);

		// Re-calculate scale mode and update screen size. This only applies if
        // ScaleMode is not set to RESIZE.
        this.scale.refresh();

		//Inicia las fisicas con el motor 'ARCADE'.
		this.game.physics.startSystem(Phaser.Physics.ARCADE);
		this.state.start('Preload');
	},

};