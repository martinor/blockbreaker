var BlockBreaker = BlockBreaker || {};

BlockBreaker.MainMenu = function() {};
var theme = localStorage.getItem('theme') || 1;
var maxScore = localStorage.getItem('maxscore') || 0;

BlockBreaker.MainMenu.prototype = {
    create: function() {
        // Añadir fondo del logo
        this.logo = this.add.sprite(
            this.world.centerX, // (centerX, centerY) centro en ejes X, Y
            this.world.centerY,
            'logo');
        // Punto de anclaje al centro de la pantalla
        this.logo.anchor.setTo(0.5, 0.5);
        this.background = this.add.tileSprite(0, 0, 800, 600, 'starfield');
        this.add.button(630, 500, 'button-start', this.startGame, this);
        this.add.button(680, 50, 'button-config', this.configGame, this);
        
        //Añade el sonido.
        /*this.theme = this.add.audio('mainmenu-theme');
        this.theme.play();*/
    },

    //Inicia el juego.
    startGame: function() {
        this.state.start('Game');
    },

    //Menu de configuraciones.
    configGame: function() {
        this.state.start('ConfigGame');
    },

    /*render: function() {
        this.game.debug.soundInfo(this.theme, 20, 32);
    },*/
};