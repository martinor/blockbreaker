//Crea la clase BlockBreaker
var BlockBreaker = BlockBreaker || {};

//Crea la funcion 'Preload' en la variable BlockBreaker
BlockBreaker.Preload = function() {};

//Inicializa la funcion 'Preload' en un prototipo.
BlockBreaker.Preload.prototype = {
	preload: function() {
		//Mostrar pantalla de carga.
		this.preloadBar = this.add.sprite(
			this.game.world.centerX,
			this.game.world.centerY,
			'preloadbar');
		this.preloadBar.anchor.setTo(0.5);
		//this.preloadBar.scale.setTo(2);
		this.load.setPreloadSprite(this.preloadBar);

		//Carga los assets de la interfaz -- TEMA = 0.
		this.load.image('button-start', 'asset/UI/buttons/40.png');
		this.load.image('button-config', 'asset/UI/buttons/20.png');
		this.load.image('button-pause', 'asset/UI/buttons/12.png');
        this.load.image('button-back', 'asset/UI/buttons/19.png');
        this.load.image('button-reboot', 'asset/UI/buttons/52.png');
        this.load.image('button-menu', 'asset/UI/buttons/31.png');
		this.load.image('button-left', 'asset/UI/buttons/22.png');
		this.load.image('button-right', 'asset/UI/buttons/23.png');
		this.load.image('button-yes', 'asset/UI/buttons/44.png');
		this.load.image('button-no', 'asset/UI/buttons/45.png');
		//this.load.atlasXML('greyGUI', 'asset/UI/greySheet.png', 'asset/UI/greySheet.xml');
		//this.load.atlasXML('blueGUI', 'asset/UI/blueSheet.png', 'asset/UI/blueSheet.xml');
		this.load.image('panel-back', 'asset/UI/panel.png');
		this.load.image('selected-back', 'asset/UI/selected.png');
		this.load.image('logo', 'asset/logo.png');
        
        //Vista previa de los temas.
        this.load.image('preview-0', 'asset/InGame/themes/0/modern.png');
        this.load.image('preview-1', 'asset/InGame/themes/1/pixel.png');
        this.load.image('preview-2', 'asset/InGame/themes/2/classic.png');

		//Carga los sonidos del juego.
		this.load.audio('mainmenu-theme', 'asset/sounds/Adventure.mp3');
		//this.load.audio('ingame-theme', 'asset/sounds/jump to win.mp3');
		//this.load.audio('levelup-theme', 'asset/sounds/chipquest.wav');
		//this.load.audio('brickhit-theme', 'asset/sounds/switch2.ogg');
		//this.load.audio('paddlehit-theme', 'asset/sounds/switch3.ogg');

		//Carga los assets del juego.
		this.load.image('starfield', 'asset/starfield.jpg');


        //Efectos de particulas.
        this.load.image('blue-spark', 'asset/InGame/particles/particleStar.png');
        this.load.image('fire1', 'asset/InGame/particles/fire1.png');
        this.load.image('fire2', 'asset/InGame/particles/fire2.png');
        this.load.image('fire3', 'asset/InGame/particles/fire3.png');
        this.load.image('smoke', 'asset/InGame/particles/smoke-puff.png');
	},

	create: function() {
		this.state.start('MainMenu');
	}

};