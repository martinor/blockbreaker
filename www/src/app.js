(function () {
    var game = new Phaser.Game(800, 600, Phaser.CANVAS, '');
    game.state.add('Boot', BlockBreaker.Boot);
    game.state.add('Preload', BlockBreaker.Preload);
    game.state.add('MainMenu', BlockBreaker.MainMenu);
    game.state.add('ConfigGame', BlockBreaker.ConfigGame);
    game.state.add('Game', BlockBreaker.Game);
    game.state.start('Boot');
})();