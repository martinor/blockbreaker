//Crea la clase BlockBreaker
var BlockBreaker = BlockBreaker || {};

//Crea la funcion 'Game' en la variable BlockBreaker
BlockBreaker.Game = function() {};

//Inicializa la funcion 'Game' en un prototipo.
BlockBreaker.Game.prototype = {
    preload: function() {
        this.game.time.advancedTiming = true;
        
        //Mostrar pantalla de carga.
		this.preloadBar = this.add.sprite(
			this.game.world.centerX,
			this.game.world.centerY,
			'preloadbar');
		this.preloadBar.anchor.setTo(0.5);
		//this.preloadBar.scale.setTo(2);
		this.load.setPreloadSprite(this.preloadBar);
        
        this.load.image('paddle', 'asset/InGame/themes/'+theme+'/paddle.png');
        this.load.image('brick0', 'asset/InGame/themes/'+theme+'/brick0.png');
        this.load.image('brick1', 'asset/InGame/themes/'+theme+'/brick1.png');
        this.load.image('brick2', 'asset/InGame/themes/'+theme+'/brick2.png');
        this.load.image('brick3', 'asset/InGame/themes/'+theme+'/brick3.png');
        this.load.image('brick4', 'asset/InGame/themes/'+theme+'/brick4.png');
        this.load.image('brick5', 'asset/InGame/themes/'+theme+'/brick5.png');
        this.load.image('ball', 'asset/InGame/themes/'+theme+'/ball.png');
    },
    
    create: function() {
        //Añadir fondo
        if(theme != 2) {
            this.background = this.add.tileSprite(0, 0, 800, 600, 'starfield');
            this.background.autoScroll = true;
        }
        
        //Agrega los botones de manejo touch (Para dispositivos compatibles).
        this.touchLeft = this.add.sprite(32, 450, 'button-left');
        this.touchRight = this.add.sprite(680, 450, 'button-right');
        
        this.pauseButton = this.add.button(680, 50, 'button-pause', this.pauseMenu, this);
        
        this.rebootButton = this.add.button(680, 50, 'button-reboot', this.rebootGame, this);
        this.menuButton = this.add.button(630, 50, 'button-menu', this.mainMenu, this);
        this.rebootButton.visible = false;
        this.menuButton.visible = false;
        
        //Inicia las fisicas de PhaserJS.
        this.physics.startSystem(Phaser.Physics.ARCADE);
        //Verifica las fisicas en todas las secciones menos en la parte inferior.
        this.physics.arcade.checkCollision.down = false;

        //Variable que se encarga del movimiento
        this.cursor = this.input.keyboard.createCursorKeys();
        this.touchLeft.inputEnabled = true;
        this.touchRight.inputEnabled = true;
        
        //Valores iniciales para nivel, vida y puntuacion.
        this.lvlMax = 1;
        this.livesPoints = 3;
        this.scoreMax = 0;
        
        //conteo de toques entre la pelota y un bloque.
        this.ballHits = 0;
        
        //Crea los textos que se mostraran durante el juego.
        this.gameText = this.add.text(this.game.world.centerX, 400, '', { font: "20px Arial", fill: "#ffffff", align: "center" });
        this.gameText.anchor.setTo(0.5, 0.5);
        this.scoreText = this.add.text(32, 550, 'Puntuacion: 0', { font: "20px Arial", fill: "#ffffff", align: "left" });
        this.livesText = this.add.text(680, 550, 'Vidas: 3', { font: "20px Arial", fill: "#ffffff", align: "left" });
        this.scoremaxText = this.add.text(32, 50, 'Puntuacion maxima: ' + maxScore, { font: "20px Arial", fill: "#ffffff", align: "left" });

        //Crea el objeto 'paddle' y la pelota.
        this.paddle = this.add.sprite(this.world.centerX, 500, 'paddle');
        this.ball = this.add.sprite(this.world.randomX, 350, 'ball');

        //Inicia las fisicas para el objeto 'paddle'
        this.physics.arcade.enable(this.paddle);

        //Detiene el movimiento de 'paddle' mientras toca la pelota.
        this.paddle.body.immovable = true;
        this.paddle.body.collideWorldBounds = true;

        //Posiciona todos los ladrillos para romper.
        this.bricks = this.add.group();
        this.bricks.enableBody = true;
        this.newBricks();
        
        this.setBall();

        this.emitter = this.add.emitter(0, 0, 100);
        this.emitter.makeParticles('blue-spark');
        this.emitter.gravity = 200;
        
        this.fire = this.add.emitter(0, 0, 100);
        this.fire.makeParticles([ 'fire1', 'fire2', 'fire3', 'smoke' ]);
        this.fire.gravity = 200;
        this.fire.setAlpha(1, 0, 3000);
        this.fire.setScale(0.1, 0, 0.1, 0, 30);
    },

    update: function() {
        //Logica del juego, ejecutado cada 60 frames.

        //Si se presiona la tecla de la derecha, el 'paddle' se movera en esa direccion.
        if(this.cursor.right.isDown)
            this.paddle.body.velocity.x = 800;

        //Si se presiona la izquierda, pues se movera hacia la izquierda.
        else if(this.cursor.left.isDown)
            this.paddle.body.velocity.x = -800;

        //Si no se presiona una tecla, pues no se movera.
        else
            this.paddle.body.velocity.x = 0;

        if(this.input.pointer1.isDown) {
            if(Math.floor(this.input.x/(this.game.width/2)) === 1 )
                //Mueve el 'paddle' a la izquierda.
                this.paddle.body.velocity.x = 800;
            else if(Math.floor(this.input.x/(this.game.width/2)) === 0 )
                //Mueve el 'paddle' a la derecha.
                this.paddle.body.velocity.x = -800;
        }

        if(this.ballHits > 4) {
            var px = this.ball.body.velocity.x;
            var py = this.ball.body.velocity.y;

            px *= -1;
            py *= -1;

            this.fire.emitX = this.ball.body.x;
            this.fire.emitY = this.ball.body.y;
        } else {
            this.fire.on = false;   
        }

        //Permitir colision entre la pelota y el 'paddle'.
        this.physics.arcade.collide(this.ball, this.paddle, this.paddleHit, null, this);

        //Llama la funcion 'hit' durante la colision.
        this.physics.arcade.collide(this.ball, this.bricks, this.brickHit, null, this);
    },

    setBall: function() {
        this.gameText.text = "¡Preparate!";
        this.game.add.tween(this.gameText).to( { alpha: 1 }, 1000, Phaser.Easing.Linear.None)
        .to( { alpha: 0 }, 500, Phaser.Easing.Linear.None, true)
        .to( { alpha: 1 }, 1000, Phaser.Easing.Linear.None, true)
        .to( { alpha: 0  }, 500, Phaser.Easing.Linear.None, true)
        .to( { alpha: 1 }, 1000, Phaser.Easing.Linear.None, true)
        .start();

        this.initBall = this.game.time.events.add(Phaser.Timer.SECOND * 4, this.start, this);
        this.initBall.timer.start();
    },

    start: function() {
        //Añadir fisicas a la pelota.
        

        this.emitter.x = this.ball.x;
        this.emitter.y = this.ball.y;
        this.emitter.start(true, 1000, null, 3);
        
        this.physics.arcade.enable(this.ball);

        //Añadir velocidad a la pelota.
        this.ball.body.velocity.x = 600;
        this.ball.body.velocity.y = 600;

        //Hacer a la pelota 'rebotar'.
        this.ball.collideWorldBounds = true;
        this.ball.body.bounce.x = 1;
        this.ball.body.bounce.y = 1;
        
        //Evita que la Pelota se salga de los limites en las paredes.
        this.ball.body.collideWorldBounds = true;
        
        //Activa el evento de perdida de vida una vez la pelota sale los limites.
        this.ball.checkWorldBounds = true;
        this.ball.events.onOutOfBounds.add(this.ballLost, this);
        
        this.gameText.visible = false;
    },

    brickHit: function(ball, brick) {
        //Destruye un objeto 'brick'
        if(brick.doubleHit) {
            this.game.add.tween(brick).to( { alpha: 0.5 }, 500, Phaser.Easing.Linear.None).start();
            brick.doubleHit = false;
        } else {
            brick.kill();
        }
        
        //Añade el puntaje y lo suma en pantalla.
        this.scoreMax += 10;
        this.scoreText.text = 'Puntuacion: ' + this.scoreMax;

        this.ballHits ++;
        if(this.ballHits == 5) {
            //Inicia el emisor de particulas
            this.fire.start(false, 3000, 1);
            
            this.ball.body.velocity.x *= 1.1;
            this.ball.body.velocity.y *= 1.1;
        }
        
        //Verifica si se acabaron los ladrillos.
        if (this.bricks.countLiving() === 0) {
            
            //Nuevo nivel
            this.scoreMax += 1000;
            this.livesPoints ++;
            this.lvlMax ++;
            this.scoreText.text = 'Puntuacion: ' + this.scoreMax;
            this.livesText.text = 'Vida: ' + this.livesPoints;
            
            this.ball.reset(this.paddle.x, 250);
            this.ball.body.velocity.y = 500;
            this.ball.body.velocity.x = 500;
            this.gameText.visible = true;
            this.gameText.text = "Nuevo nivel";
            this.newBricks();
            this.setBall();
        }

        if(this.scoreMax > maxScore) {
            localStorage.setItem('maxscore', this.scoreMax);
            maxScore = localStorage.getItem('maxscore');
            this.scoremaxText.text = 'Puntuacion maxima: ' + maxScore;
        }
    },

    newBricks: function() {
        var i;
        var j;
        if(theme != 2 ) {
            this.rndLvl = this.rnd.integerInRange(0, 4);
            switch(this.rndLvl) {
                case 0:
                    for (i = 0;  i < 4; i++)
                        for (j = 0; j < 15; j++) {
                            this.add.sprite(120 + (j * 36), 100 + (i * 52),
                                'brick'+this.rnd.integerInRange(0, 5), 0, this.bricks );
                            this.bricks.doubleHit = true;
                        }
                    break;

                case 1:
                    var color = this.rnd.integerInRange(0, 5);
                    for (i = 0;  i < 12; i++)
                        for (j = 0; j < 8; j++) {
                            this.add.sprite(50 + i * 60, 100 + j * 35,
                                'brick'+color, 0, this.bricks );
                            this.bricks.doubleHit = true;
                        }
                    break;

                case 2:
                    for (i = 0;  i < 5; i++)
                        for (j = 0; j < 20; j+=3) {
                            this.add.sprite(80 + (j * 40), 100 + (i * 52),
                                'brick'+this.rnd.integerInRange(0, 5), 0, this.bricks );
                            if(0 === this.rnd.integerInRange(0, 1))
                                this.bricks.doubleHit = true;
                        }
                    break;

                case 3:
                    for (i = 0;  i < 4; i++)
                        for (j = 0; j < 15; j++) {
                            this.add.sprite(120 + (j * 36), 100 + (i * 52),
                                'brick'+this.rnd.integerInRange(0, 5), 0, this.bricks );
                            this.bricks.doubleHit = true;
                            if(0 === this.rnd.integerInRange(0, 1))
                                j += 2;
                        }
                    break;
                case 4:
                    var customRnd = this.rnd.integerInRange(5, 20);
                    for (i = 0;  i < customRnd; i++)
                        for (j = 0; j < customRnd; j++) {
                            this.add.sprite(120 + (j * 36), 100 + (i * 52),
                                'brick'+this.rnd.integerInRange(0, 5), 0, this.bricks );
                            this.bricks.doubleHit = true;
                            j += this.rnd.integerInRange(1, 5);
                            i += this.rnd.integerInRange(1, 5);
                        }
                    break;
            }
        } else {
            this.rndLvl = this.rnd.integerInRange(0, 1);
            switch(this.rndLvl) {
                case 0:
                    for (i = 0;  i < 4; i++)
                        for (j = 0; j < 15; j++)
                            this.add.sprite(120 + (j * 36), 100 + (i * 52),
                                'brick1', 0, this.bricks );
                    break;

                case 1:
                    for (i = 0;  i < 12; i++)
                        for (j = 0; j < 8; j++)
                            this.add.sprite(50 + i * 60, 100 + j * 35,
                                'brick1', 0, this.bricks );
                    break;
            }
        }
        this.bricks.setAll('body.immovable', true);
    },
    
    paddleHit: function(ball, paddle) {
        //Reinicia el blockhit powerUp
        this.ballHits = 0;
        
        var diff = 0;
        if (ball.x < paddle.x) {
            //Cuando la pelota se encuentre mas a la izquierda, en el impacto se movera a ese lado.
            diff = paddle.x - ball.x;
            ball.body.velocity.x = ball.body.velocity.x + (-10 * diff);
        } else if (ball.x > paddle.x) {
            //Cuando la pelota se encuentre mas a la derecha, en el impacto se movera a ese lado.
            diff = ball.x - paddle.x;
            ball.body.velocity.x = ball.body.velocity.x + (10 * diff);
        } else {
            //Si la pelota esta en el centro, se movera a un lado aleatorio.
            ball.body.velocity.x = 2 + Math.random() * 8;
        }
    },
    
    ballLost: function() {
        this.ballHits = 0;
        
        this.livesPoints--;
        this.livesText.text = 'Vidas: ' + this.livesPoints;
        
        if (this.livesPoints === 0)
            this.gameOver();
        else
            this.ball.reset(this.paddle.x, 350);
            this.ball.body.velocity.y = 500;
            this.ball.body.velocity.x = 500;
    },
    
    gameOver: function() {
        this.ball.body.velocity.setTo(0, 0);
        
        this.gameText.visible = true;
        this.gameText.text = "Fin del juego";
        
        //Oculta los botones inecesarios durente el gameOver
        this.touchLeft.visible = false;
        this.touchRight.visible = false;
        this.pauseButton.visible = false;
        
        this.rebootButton.visible = true;
        this.menuButton.visible = true;
    },
    
    rebootGame: function() {
        //Reinicia los valores de nivel, puntuacion y vida.
        this.lvlMax = 1;
        this.livesPoints = 3;
        this.scoreMax = 0;
        this.ballHits = 0;
        
        //Muestra los nuevos valores en pantalla
        this.livesText.text = 'Vidas: ' + this.livesPoints;
        this.scoreText.text = 'Puntuacion: ' + this.scoreMax;
        
        this.ball.reset(this.world.randomX, 350);
        this.setBall();
        
        //Reinicia la posicion del objeto 'paddle'.
        this.paddle.reset(this.world.centerX, 500);
        //Llama a los bloques destruidos a su posicion inicial.
        this.bricks.callAll('kill');
        this.newBricks();
        
        //Oculta el boton de reinicio, menu y el texto de 'Fin del juego'.
        this.menuButton.visible = false;
        this.rebootButton.visible = false;
        //Muestra lo ocultado en el gameOver.
        this.touchLeft.visible = true;
        this.touchRight.visible = true;
        this.pauseButton.visible = true;
    },
    
    mainMenu: function() {
        this.state.start('MainMenu');  
    },
    
    pauseMenu: function() {
        this.game.paused = true;
        this.gameText.visible = true;
        this.gameText.text = "Pausado.\nToque la pantalla para continuar.\npara salir debe perder todas sus vidas.";
        this.pauseButton.visible = false;
        
        this.input.onDown.add(this.unPause, this);
    },
            
    unPause: function() {
        if(this.game.paused) {
            this.gameText.visible = false;
            this.pauseButton.visible = true;
            this.game.paused = false;
        }
    },

    /*render: function() {
        this.game.debug.text(this.game.time.fps || '--', 20, 70, "#00ff00", "40px Courier");
        this.game.debug.inputInfo(32, 32);
    },*/
};