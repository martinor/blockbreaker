var BlockBreaker = BlockBreaker || {};

BlockBreaker.ConfigGame = function() {};

BlockBreaker.ConfigGame.prototype = {
    create: function() {
        this.background = this.add.tileSprite(0, 0, 800, 600, 'starfield');
        this.add.button(680, 50, 'button-back', this.backToMain, this);
        
        this.modernTheme = this.add.sprite(
            this.world.centerX - (this.world.centerX/2),
            this.world.centerY,
            'preview-0');
        this.modernTheme.anchor.setTo(0.2, 0.5);
        this.modernTheme.variable = 0;
        this.modernTheme.inputEnabled = true;
        this.modernTheme.events.onInputDown.add(this.saveChanges, this);

        this.pixelTheme = this.add.sprite(
            this.world.centerX,
            this.world.centerY,
            'preview-1');
        this.pixelTheme.anchor.setTo(0.5, 0.5);
        this.pixelTheme.variable = 1;
        this.pixelTheme.inputEnabled = true;
        this.pixelTheme.events.onInputDown.add(this.saveChanges, this);

        this.classicTheme = this.add.sprite(
            this.world.centerX + (this.world.centerX/2),
            this.world.centerY,
            'preview-2');
        this.classicTheme.anchor.setTo(0.8, 0.5);
        this.classicTheme.variable = 2;
        this.classicTheme.inputEnabled = true;
        this.classicTheme.events.onInputDown.add(this.saveChanges, this);

    },
    
    backToMain: function() {
        this.state.start('MainMenu');   
    },

    saveChanges: function(choice) {
        localStorage.setItem('theme', choice.variable);
        theme = localStorage.getItem('theme');
        this.state.start('MainMenu');
    }
};